# Require any additional compass plugins here.

# Set this to the root of your project when deployed:
http_path = "output/"
css_dir = "output/"
sass_dir = "content/sass/"
images_dir = "output/assets"
javascripts_dir = "output/"
fonts_dir = "output/assets/fonts"

output_style = :expanded
environment = :development

relative_assets = false

# To disable debugging comments that display the original location of your selectors. Uncomment:
# line_comments = false
color_output = false


# If you prefer the indented syntax, you might want to regenerate this
# project again passing --syntax sass, or you can uncomment this:
# preferred_syntax = :sass
# and then run:
# sass-convert -R --from scss --to sass static/sass scss && rm -rf sass && mv scss sass
preferred_syntax = :scss
