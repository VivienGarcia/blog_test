# Defaults
include Nanoc3::Helpers::Rendering
include Nanoc3::Helpers::Blogging
include Nanoc3::Helpers::Tagging
include Nanoc3::Helpers::LinkTo


module PostHelper
	def get_page(page)
		page = @item[:slug]
		return page
	end

	def get_pretty_date(arg)
		attribute_to_time(arg).strftime('%d-%m-%Y à %kh%M')
	end

	def get_post_start(post)
		content = post.compiled_content
		if content =~ /<!-- more -->/
			content = content.partition('<!-- more -->').first
		end
		return content
	end

	def next_link
		prv = sorted_articles.index(@item) - 1
		if sorted_articles[prv].nil?
			return ""
		else
			link_to( 'Suivant &rarr;', sorted_articles[prv].reps[0])
		end
	end

	def previous_link
		nxt = sorted_articles.index(@item) + 1
		if sorted_articles[nxt].nil?
			return ""
		else
			link_to('&larr; Précédent ', sorted_articles[nxt].reps.find { |r| r.name == :default })
		end
	end

end

module Tags
	def all_tags
		tags = []
		sorted_articles.each do |item|
			next if item[:tags].nil?
			item[:tags].each { |tag| tags << tag }
		end
		tags.uniq
	end

	def articles_with_tag(tag)
	  # sorted_articles.select{ |a| a[:tags].include?(tag) rescue false }
	  a = sorted_articles.select{ |a| a[:tags].include?(tag) rescue puts 'article_with_tag a planté' }
	  puts a
	end
end

module MetaHelpers
	def site_url()
		return site.config[:meta_data][:site_url]
	end

	def blog_url()
		return site.config[:meta_data][:blog_url]
	end

	def site_assets_url()
		return site.config[:meta_data][:assets_url]
	end

	def site_medias_url()
		return site.config[:meta_data][:medias_url]
	end
end



include PostHelper
include Tags
include MetaHelpers