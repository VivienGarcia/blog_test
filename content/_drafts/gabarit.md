---
title: Titre du blog
<!-- slug: fonction de la page -->
<!-- created_at: 2000-12-31 24:59:59 +0100 -->
<!-- modified_at: 2000-12-31 24:59:59 +0100 -->
<!-- kind: article -->
---
##INSTRUCTIONS
**Attention a l'heure de création : "+0100" en hiver et "+0200" en été**

***slug* : utilisé pour les pages "statiques"** 

***modified_at* prend la valeur de *created_at* et reste en commentaire jusqu'à ce que l'article subisse une modification**

***tags* n'est utilisé que pour les articles**

**L'excerpt n'est utilisé que pour les articles de blog**
<!-- more -->

Suite de l'article.