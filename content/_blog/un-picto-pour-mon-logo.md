---
title: Un picto pour mon logo
created_at: 2013-09-04 12:57:00 +0200
modified_at: 2013-09-04 15:51:00 +0200
kind: article
---
Ça fait un moment que j'ai décidé de refaire mon identité visuelle, et donc mon logo.
<!-- more -->

Un logo, c'est constitué de plusieurs éléments, pas tous indispensables.  
Il y a l'élément pictural ou pictogramme (comme la virgule de nike) et la marque avec sa police souvent personnalisée.

En fonction des situations on peut se contenter du pictogramme ou de la marque, si l'emplacement est inadapté pour des raisons physiques ou stylistiques).

Voici un aperçu de mon nouveau logo :

<figure class="item--media__article">
    <img src="_medias/picto.png" alt="Image de mon pictogramme inachevé" title="Pictogramme - Non définitif" width="204" height="240" />
</figure>