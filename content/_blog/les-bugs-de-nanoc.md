---
title: Les bugs de Nanoc
created_at: 2013-08-30 11:20:00 +0200
modified_at: 2013-08-30 11:20:00 +0200
kind: article
---
La commande ```guard``` ne compile pas tous les éléments dans certains cas.
<!-- more -->

Éléments concernés : ```<ul>```, ```<br>```, ```<hr>```.

Circonstances : l'élément ```<ul>``` semble être interpreté quand il n'est pas  en présences d'inclusions ```erb```.