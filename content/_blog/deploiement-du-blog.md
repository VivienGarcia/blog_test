---
title: Déploiement du blog
created_at: 2013-09-18 17:55:00 +0200
modified_at: 2013-09-18 17:55:00 +0200
kind: article
---
Continuant mes projet de blog statique, je teste plusieurs moyens de déploiement qui permettent l'utilisation d'un système de contrôle de versions.
<!-- more -->

Le contrôle de versions sera effectué via git, reste à trouvcer une façon intéressante de déployer le site. Sachant que seul le contenu du dossier output m'intéresse, plusieurs possibilités s'offrent à moi mais je souhaite automatiser les étapes au maximum ce qui va me conduire à de nombreuses recherche, pour changer.